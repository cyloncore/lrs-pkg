require 'optparse'
require 'ostruct'
require 'fileutils'
require 'pty'

require_relative 'settings'
Settings.load!("#{Dir.home()}/.lrspkgrc")

require_relative 'helpers'
require_relative 'errors'
require_relative 'commands'
require_relative 'recipes'
require_relative 'platforms'
require_relative 'packages'
require_relative 'packagers'
require_relative 'colorize'
require_relative 'processor_count.rb'

unless(ENV.include?('CC_PKG_ROOT'))
  puts "Undefined environment variable CC_PKG_ROOT."
  exit -1
end

CC_PKG_DIR = File.dirname(__FILE__) 

args_array = ARGV

if(args_array.size == 0)
  command = 'help'
else
  command = args_array[0]
  args_array.shift
end

########################################################################
# Handle the switch of checkout from src/category/package to src/package
has_moved = false
AvailablePackages.each() do |name,package|
  oldcheckoutdir = "#{ENV['CC_PKG_ROOT']}/src/#{package.category}/#{name}"
  if(File.directory?(oldcheckoutdir) and oldcheckoutdir != package.checkout_directory and package.category != "")
    if(File.exist?(package.checkout_directory))
      puts("Error: package #{name} has both an old (src/#{package.category}/#{name}) and a new checkout (src/#{name}), please remove one!".red)
      exit -1
    else
      has_moved = true
      puts("Moving #{name} from src/#{package.category}/#{name} to src/#{name}...".yellow)
      FileUtils.mv(oldcheckoutdir, package.checkout_directory)
    end
  end
end

# In case a package has been moved, clean
if(has_moved)
  answer = nil
  until answer == "y" or answer == "n"
    puts("Some checkouts have been moved, it is recommanded to clean, do you want to do it now? (y/n)".light_blue)
    answer = gets.chomp
  end
  if answer == "y"
    `rm -rf #{ENV['CC_PKG_ROOT']}/build-debug/ #{ENV['CC_PKG_ROOT']}/build-release/ #{ENV['CC_PKG_ROOT']}/inst/`
  end
  # Remove empty category directory
  AvailablePackages.each() do |name,package|
    categorydir = "#{ENV['CC_PKG_ROOT']}/src/#{package.category}"
    if(File.exist?(categorydir) and package.category != "")
      begin
        Dir.rmdir(categorydir)
      rescue
        puts("Failed to remove #{categorydir} directory which should have been empty, remove manually".red)
      end
    end
  end
end

########################################################################
# Handle the migration from HG to GIT

AvailablePackages.each() do |name,package|
  if(package.vc == Git and File.exist?(package.checkout_directory()) and not File.exist?(package.checkout_directory() + "/.git"))
    puts("Migrating #{name} from HG to GIT".yellow)
    FileUtils.remove_dir(package.checkout_directory() + "/.hg")
    bare_dir = "#{package.checkout_directory()}/__bare__git__dir"
    `git clone #{package.url} #{bare_dir}`
    FileUtils.mv(bare_dir + "/.git", package.checkout_directory() + "/.git")
    FileUtils.remove_dir(bare_dir)
    `cd #{package.checkout_directory()}; git stash`
    `cd #{package.checkout_directory()}; git reset --hard`
  end
end

########################################################################
# Handle change of branches

AvailablePackages.each() do |name,package|
  if package.vc == Git and File.exist?(package.checkout_directory()) and package.option(:vc, :switch_branch) != nil
    switch_branch = package.option(:vc, :switch_branch)
    package.vc_instance.switch_branch *switch_branch
  end
end
  
################
# handle command

if(AvailableCommands.include?(command))
  kommand = AvailableCommands[command]
  
  options = OpenStruct.new
  opt_parser = OptionParser.new
  
  kommand.parse_options(opt_parser, options)
  
  opt_parser.parse!(args_array)
  
  options.args = args_array
  
  kommand.new(options).execute()
else
  Errors.unknown_command(command)
end
