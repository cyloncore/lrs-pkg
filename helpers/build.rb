module Helpers
  
  class Build
    def initialize(platform, build_mode, force_configure, even_explicits)
      platform.check_env()
      @already_build    = []
      @platform         = platform
      @build_mode       = build_mode
      @force_configure  = force_configure
      @even_explicits   = even_explicits
    end
    def recursive_build(package)
      return if(@already_build.include?(package.name))
      # Build dependencies
      package.all_installed_dependencies().each() do |d_name|
        dep_package = Packages.get_package(d_name)
        recursive_build(dep_package) if @even_explicits or not dep_package.option(:command, :build, :explicit, default_value: false)
      end
      build(package)
    end
    def build(package)
      # Now build the current package
      if(not @already_build.include?(package.name) and package.is_enabled?(@platform))
        @already_build << package.name
        puts "Building #{package.name}".light_blue
        recipe = package.recipe_instance(@platform, @build_mode)
        if(@force_configure or not recipe.configured?)
          recipe.configure
        end
        recipe.build
        if(recipe.has_install?)
          recipe.install
        end
      end
    end

  end
  
end
