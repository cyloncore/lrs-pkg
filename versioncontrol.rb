class VersionControl
  attr_reader :url, :checkout_directory, :options, :patches
  def initialize(url, checkout_directory, options, patches)
    @url = url
    @checkout_directory = checkout_directory
    @options = options
    @patches = patches
  end
  
protected
  def apply_patches()
    @patches.each() { |patch|
      puts "Applying patch: #{patch}".light_blue
      VersionControl.run_command("cd #{self.checkout_directory}; patch -p 1 < #{patch}")
    }
  end
  def VersionControl.run_command(command)
    output = `#{command}`
    if($?.exitstatus != 0)
      puts "Running command #{command} has failed, with output:".red
      puts output
      exit -1
    end
    return output
  end
end

class Hg < VersionControl
  def initialize(url, checkout_directory, options, patches)
    super
  end
  # Clone the repository
  def clone()
    return VersionControl.run_command("hg clone #{self.url} #{self.checkout_directory}")
  end
  def updateRemote()
  end
  # Get data from the repository, and update to latest revision
  def fetch()
    return VersionControl.run_command("cd #{self.checkout_directory}; hg pull; hg rebase; hg update")
  end
  # Get tags from the repository
  def tags()
    tags = `cd #{self.checkout_directory}; hg tags`
    t = []
    tags.split("\n").each() { |l|
      t << /([^ ]+).*$/.match(l)[1]
    }
    return t
  end
  def cat(filename, version)
    return `cd #{self.checkout_directory}; hg cat -r #{version} #{filename}`
  end
  def archive(dst, dir, version)
    run_command("cd #{self.checkout_directory}; hg archive -r #{version} -p #{dir} -t tbz2 #{dst}")
  end
  def has_outgoing()
    outgoing_str = `cd #{self.checkout_directory}; hg outgoing`
    return false if(outgoing_str.include?("searching for changes\nno changes found\n"))
    return true
  end
  def has_uncommited()
    return `cd #{self.checkout_directory}; hg status` != ''
  end
end
 
class Git < VersionControl
  def initialize(url, checkout_directory, options, patches)
    super
  end
  # Clone the repository
  def clone()
    r = VersionControl.run_command("git clone --recursive #{self.url} #{self.checkout_directory}")
    if(self.options.include?(:tag))
      VersionControl.run_command("cd #{self.checkout_directory}; git checkout #{self.options[:tag]}")
    elsif(self.options.include?(:branch))
      VersionControl.run_command("cd #{self.checkout_directory}; git checkout -b #{self.options[:branch]} --track origin/#{self.options[:branch]}")
    end
    if(self.options.include?(:push_url))
      r = VersionControl.run_command("cd #{self.checkout_directory}; git remote set-url --push origin #{self.options[:push_url]}")
    end
    apply_patches()
    return r
  end
  def switch_branch(from, to)
    current = `cd #{self.checkout_directory}; git branch --show-current`.chomp
    if current == from
      `cd #{self.checkout_directory}; git fetch --all; git checkout #{to}`
    end
  end
  def remote_url
    return `cd #{self.checkout_directory}; git remote get-url origin`.chomp
  end
  def remote_push_url
    return `cd #{self.checkout_directory}; git remote get-url --push origin`.chomp
  end
  def updateRemote()
    remote_uri = `cd #{self.checkout_directory}; git config remote.origin.url`.chomp
    if(remote_uri != self.url)
      r = Git.change_remote_uri(self.checkout_directory, self.url)
    end
    if(self.options.include?(:push_url))
      push_remote_uri = `cd #{self.checkout_directory}; git config remote.origin.pushurl`.chomp
      push_url = self.options[:push_url]
      if(push_url != push_remote_uri)
        r = VersionControl.run_command("cd #{self.checkout_directory}; git remote set-url --push origin #{push_url}")
      end
    end
  end
  # Get data from the repository, and update to latest revision
  def fetch()
    unless(self.patches.empty?())
      VersionControl.run_command("cd #{self.checkout_directory}; git stash")
      puts "This repository (#{self.checkout_directory}) has patches, current changes have been stashed!".red
    end
    if(self.options.include?(:tag))
      r = VersionControl.run_command("cd #{self.checkout_directory}; git fetch --recurse-submodules; git checkout #{self.options[:tag]}")
    elsif(self.options.include?(:branch))
      r = VersionControl.run_command("cd #{self.checkout_directory}; git fetch --recurse-submodules; git checkout #{self.options[:branch]}")
    else
      r = VersionControl.run_command("cd #{self.checkout_directory}; git pull --rebase --recurse-submodules")
    end
    if(self.options.include?(:recursive) and self.options[:recursive])
      r = VersionControl.run_command("cd #{self.checkout_directory}; git submodule update --recursive --remote")
    end
    apply_patches()
    return r
  end
  def has_outgoing()
    return VersionControl.run_command("cd #{self.checkout_directory}; git status").include?('(use "git push" to publish your local commits)')
  end
  def has_uncommited()
    status_str = `cd #{self.checkout_directory}; git status`
    return (status_str.include?('Changes to be committed:') or status_str.include?('Changes not staged for commit:'))
  end
  def Git.change_remote_uri(directory, new_uri)
    return VersionControl.run_command("cd #{directory}; git remote set-url origin #{new_uri}")
  end

end

class Tar < VersionControl
  def initialize(url, checkout_directory, options, patches)
    super
  end
  def updateRemote()
  end
  def clone()
    `mkdir -p #{self.checkout_directory};cd #{self.checkout_directory}; wget #{self.url}; tar -xzf #{File.basename(self.url)}; mv */* .; rm -f #{File.basename(self.url)}`
  end
  def updateRemote()
  end
  def fetch()
  end
  def tags()
    return []
  end
  def has_outgoing()
    return false
  end
  def has_uncommited()
    return false
  end
end

class NoVersionControl < VersionControl
  def initialize(url, checkout_directory, options, patches)
    super
  end
  def updateRemote()
  end
  def clone()
    FileUtils.mkpath(self.checkout_directory)
  end
  def updateRemote()
  end
  def fetch()
  end
  def tags()
    return []
  end
  def has_outgoing()
    return false
  end
  def has_uncommited()
    return false
  end
end
