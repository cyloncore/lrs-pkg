module Platforms
  class IOSBase < Platform
    def IOSBase.check_env()
    end
    def IOSBase.build_prefix()
      return "#{prefix()}-"
    end
    def IOSBase.install_dir()
      return "#{ENV['CC_PKG_ROOT']}/#{prefix()}-inst"
    end
    def IOSBase.update_env(env)
      env['CMAKE_PREFIX_PATH'] = self.install_dir()
      if(ENV.has_key?('CMAKE_IOS_PREFIX_PATH'))
        env['CMAKE_PREFIX_PATH'] += ";" + ENV['CMAKE_IOS_PREFIX_PATH']
      end
      env['PKG_CONFIG_PATH'] = self.install_dir() + '/lib/pkgconfig'
      path = "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:#{ENV['CC_PKG_ROOT']}/inst/bin"
      if(env.has_key?('PATH'))
        env['PATH'] += ":#{path}"
      else
        env['PATH'] = path
      end
      if(ENV.has_key?('IOS_PATH'))
        env['PATH'] = ENV['IOS_PATH'] + ":" + env['PATH']
      end
    end
    def IOSBase.cmake_arguments(recipe)
      cep = "-DIOS_DEPLOYMENT_TARGET=11.0 -DIOS_ARCH=arm64 -DCMAKE_TOOLCHAIN_FILE=#{CC_PKG_DIR}/data/cmake/toolchain/ios.toolchain.cmake -DIOS_PLATFORM=#{ios_platform()} -DCMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH"
      h = recipe.package.options(:platform, :ios, :cmake)
      if(name() != "ios")
        h = h.merge(recipe.package.options(:platform, name().to_s(), :cmake))
      end
      h.each() do |k,v|
        unless(v.nil?)
          cep += " -D#{k}=#{v}"
        end
      end
      return cep
    end
  end
end
