require_relative 'ios_base'

module Platforms
  class IOS < IOSBase
    def IOS.name()
      return 'ios'
    end
    def IOS.prefix()
      return 'ios'
    end
    def IOS.ios_platform()
      return "OS64"
    end
  end
end

AvailablePlatforms['ios'] = Platforms::IOS
