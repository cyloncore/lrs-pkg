module Platforms
  class Host < Platform
    def Host.name()
      return 'host'
    end
    def Host.group()
      return 'host'
    end
    def Host.install_dir()
      return "#{ENV['CC_PKG_ROOT']}/inst"
    end
  end
end

AvailablePlatforms['host'] = Platforms::Host
