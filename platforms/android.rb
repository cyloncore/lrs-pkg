module Platforms
  class AndroidBase < Platform
    def AndroidBase.group()
      return "android"
    end
    def AndroidBase.check_env()
      Platform.check_env_variable("ANDROID_NDK_ROOT")
      Platform.check_env_variable("ANDROID_SDK_ROOT")
    end
    def AndroidBase.build_prefix()
      return "#{prefix()}-"
    end
    def AndroidBase.install_dir()
      return "#{ENV['CC_PKG_ROOT']}/#{prefix()}-inst"
    end
    def AndroidBase.update_env(env)
      env['CMAKE_PREFIX_PATH'] = install_dir()
      env['PKG_CONFIG_PATH'] = install_dir() + '/lib/pkgconfig'
      afr = additional_find_root_path()
      afr.split(";").each() do |p|
        if p[-1] == '/'
          puts "Cannot have trailing '/' in platform ADDITIONAL_FIND_ROOT_PATH"
          exit -1
        end
      end
      env['ECM_ADDITIONAL_FIND_ROOT_PATH'] = "#{additional_find_root_path()};#{install_dir()}"
      env['ANDROID_ARCH'] = android_arch()
      env['ANDROID_API'] = "21"
      env['ANDROID_ARCH_ABI'] = android_arch_abi()
    end
    def AndroidBase.cmake_arguments(recipe)
      cep = "-DCMAKE_TOOLCHAIN_FILE:FILEPATH=#{CC_PKG_DIR}/data/cmake/toolchain/Android.cmake "
      recipe.package.options(:platform, :android, :cmake).each() do |k,v|
        unless(v.nil?)
          cep += " -D#{k}=#{v}"
        end
      end
      return cep
    end
    def AndroidBase.cmake_release_mode()
      return "Release"
    end
  end
  class Android32 < AndroidBase
    def Android32.name()
      return 'android32'
    end
    def Android32.prefix()
      return 'android32'
    end
    def Android32.additional_find_root_path()
      return ENV['ANDROID32_ECM_ADDITIONAL_FIND_ROOT_PATH']
    end
    def Android32.android_arch()
      return "arm"
    end
    def Android32.android_arch_abi()
      return "armeabi-v7a"
    end
  end
  class Android64 < AndroidBase
    def Android64.name()
      return 'android64'
    end
    def Android64.prefix()
      return 'android64'
    end
    def Android64.additional_find_root_path()
      return ENV['ANDROID64_ECM_ADDITIONAL_FIND_ROOT_PATH']
    end
    def Android64.android_arch()
      return "arm64"
    end
    def Android64.android_arch_abi()
      return "arm64-v8a"
    end
  end
  class Android86 < AndroidBase
    def Android86.name()
      return 'Android86'
    end
    def Android86.prefix()
      return 'Android86'
    end
    def Android86.additional_find_root_path()
      return ENV['ANDROID86_ECM_ADDITIONAL_FIND_ROOT_PATH']
    end
    def Android86.android_arch()
      return "x86"
    end
    def Android86.android_arch_abi()
      return "x86"
    end
  end
end

AvailablePlatforms['android32'] = Platforms::Android32
AvailablePlatforms['android64'] = Platforms::Android64
AvailablePlatforms['android86'] = Platforms::Android86
