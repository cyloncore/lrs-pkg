require_relative 'ios_base'

module Platforms
  class IOSSimulator < IOSBase
    def IOSSimulator.name()
      return 'ios-simulator'
    end
    def IOSSimulator.prefix()
      return 'ios-simulator'
    end
    def IOSSimulator.ios_platform()
      return "SIMULATOR64"
    end
  end
end

AvailablePlatforms['ios-simulator'] = Platforms::IOSSimulator
