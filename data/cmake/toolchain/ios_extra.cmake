set(CONVERT_PRL_LIBS_TO_CMAKE_PL ${CMAKE_CURRENT_LIST_DIR}/convert-prl-libs-to-cmake.pl)

function(fix_qt5_libs_dependency )

  # Parse arguments
  cmake_parse_arguments(fix_qt5_libs_dependency "" "" "COMPONENTS" ${ARGN})

  # Remove generated files
  set(STATIC_DEPENDENCIES_CMAKE_FILE "${CMAKE_BINARY_DIR}/QtStaticDependencies.cmake")

  message(STATUS "Generate Qt5 libraries dependencies in ${STATIC_DEPENDENCIES_CMAKE_FILE}")

	if(EXISTS ${STATIC_DEPENDENCIES_CMAKE_FILE})
		file(REMOVE ${STATIC_DEPENDENCIES_CMAKE_FILE})
	endif()

  # Get location of Qt libs
  get_target_property(QT_LIBDIR Qt5::Core LOCATION)
  get_filename_component(QT_LIBDIR ${QT_LIBDIR} DIRECTORY)

  # Go through the required Qt5 components

  foreach(_qt_component ${fix_qt5_libs_dependency_COMPONENTS})
    if(TARGET Qt5::${_qt_component})
      get_target_property(_lib_location Qt5::${_qt_component} LOCATION)
      execute_process(COMMAND ${CONVERT_PRL_LIBS_TO_CMAKE_PL}
        --lib ${_lib_location}
        --libdir ${QT_LIBDIR}
        --out ${STATIC_DEPENDENCIES_CMAKE_FILE}
        --component ${_qt_component}
        --compiler ${CMAKE_CXX_COMPILER_ID}
      )
    else()
      message(STATUS "${_qt_component} is *not* a Qt5 Target")
    endif()
  endforeach()

  if(NOT EXISTS ${STATIC_DEPENDENCIES_CMAKE_FILE})
  		message(FATAL_ERROR "Unable to find ${STATIC_DEPENDENCIES_CMAKE_FILE}")
  endif()
  include(${STATIC_DEPENDENCIES_CMAKE_FILE})
endfunction()
