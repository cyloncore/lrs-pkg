function(lpkg_androiddeployqt QTANDROID_EXPORTED_TARGET ANDROID_APK_DIR)

    cmake_parse_arguments(LPKG_ANDROIDDEPLOYQT "" "RELEASE" "JARS" ${ARGN})

    set(CREATEAPK_TARGET_NAME "create-apk-${QTANDROID_EXPORTED_TARGET}")

    find_package(Qt5Core REQUIRED)

    set(EXPORT_DIR "${CMAKE_BINARY_DIR}/${QTANDROID_EXPORTED_TARGET}_build_apk/")
    set(EXPORT_LIBS_DIR "${CMAKE_BINARY_DIR}/${QTANDROID_EXPORTED_TARGET}_libs_dir_apk/")
    set(EXECUTABLE_DESTINATION_PATH "${EXPORT_DIR}/libs/${CMAKE_ANDROID_ARCH_ABI}/lib${QTANDROID_EXPORTED_TARGET}.so")
    set(QML_IMPORT_PATHS "")
    foreach(prefix ${ECM_ADDITIONAL_FIND_ROOT_PATH})
        # qmlimportscanner chokes on symlinks, so we need to resolve those first
        get_filename_component(qml_path "${prefix}/lib/qml" REALPATH)
        if(EXISTS ${qml_path})
            if (QML_IMPORT_PATHS)
                set(QML_IMPORT_PATHS "${QML_IMPORT_PATHS},${qml_path}")
            else()
                set(QML_IMPORT_PATHS "${qml_path}")
            endif()
        endif()
    endforeach()
    if (QML_IMPORT_PATHS)
        set(DEFINE_QML_IMPORT_PATHS "\"qml-import-paths\": \"${QML_IMPORT_PATHS}\",")
    endif()
    set(EXTRA_PREFIX_DIRS "")
    foreach(prefix ${ECM_ADDITIONAL_FIND_ROOT_PATH})
        if (EXTRA_PREFIX_DIRS)
            set(EXTRA_PREFIX_DIRS "${EXTRA_PREFIX_DIRS}, \"${prefix}\"")
        else()
            set(EXTRA_PREFIX_DIRS "\"${prefix}\"")
        endif()
    endforeach()
    configure_file("${_CMAKE_ANDROID_DIR}/deployment-file.json.in" "${QTANDROID_EXPORTED_TARGET}-deployment.json.in")

    if (CMAKE_GENERATOR STREQUAL "Unix Makefiles")
        set(arguments "\\$(ARGS)")
    endif()
    
    function(havestl var access VALUE)
        if (NOT VALUE STREQUAL "")
            # look for ++ and .so as in libc++.so
            string (REGEX MATCH "\"[^ ]+\\+\\+[^ ]*\.so\"" OUT ${VALUE})
            file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/stl "${OUT}")
        endif()
    endfunction()
    function(haveranlib var access VALUE)
        if (NOT VALUE STREQUAL "")
            file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/ranlib "${VALUE}")
        endif()
    endfunction()
    variable_watch(CMAKE_CXX_STANDARD_LIBRARIES havestl)
    variable_watch(CMAKE_RANLIB haveranlib)
   
    if(NOT DEFINED LPKG_ANDROIDDEPLOYQT_RELEASE OR LPKG_ANDROIDDEPLOYQT_RELEASE STREQUAL "Auto")
      if(CMAKE_BUILD_TYPE STREQUAL "Release")
        set(release_arguments "--release")
      endif()
    elseif(LPKG_ANDROIDDEPLOYQT_RELEASE STREQUAL "Force")
      set(release_arguments "--release")
    endif()
    
    message(STATUS "RA ${release_arguments} AR ${arguments}")
    
    add_custom_target(${CREATEAPK_TARGET_NAME}
        DEPENDS ${ANDROID_APK_DIR}/AndroidManifest.xml
        COMMAND cmake -E echo "Generating $<TARGET_NAME:${QTANDROID_EXPORTED_TARGET}> with $<TARGET_FILE_DIR:Qt5::qmake>/androiddeployqt"
        COMMAND cmake -E remove_directory "${EXPORT_DIR}"
        COMMAND cmake -E copy_directory "${ANDROID_APK_DIR}" "${EXPORT_DIR}"
        COMMAND cmake -E copy_directory  "${EXPORT_LIBS_DIR}"  "${EXPORT_DIR}/libs/"
        COMMAND cmake -E copy "$<TARGET_FILE:${QTANDROID_EXPORTED_TARGET}>" "${EXECUTABLE_DESTINATION_PATH}"
        COMMAND LANG=C cmake -DINPUT_FILE="${QTANDROID_EXPORTED_TARGET}-deployment.json.in" -DOUTPUT_FILE="${QTANDROID_EXPORTED_TARGET}-deployment.json" "-DTARGET=$<TARGET_FILE:${QTANDROID_EXPORTED_TARGET}>" "-DOUTPUT_DIR=$<TARGET_FILE_DIR:${QTANDROID_EXPORTED_TARGET}>" "-DEXPORT_DIR=${CMAKE_INSTALL_PREFIX}" "-DECM_ADDITIONAL_FIND_ROOT_PATH=\"${ECM_ADDITIONAL_FIND_ROOT_PATH}\"" -P ${_CMAKE_ANDROID_DIR}/specifydependencies.cmake
        COMMAND $<TARGET_FILE_DIR:Qt5::qmake>/androiddeployqt --gradle --input "${QTANDROID_EXPORTED_TARGET}-deployment.json" --output "${EXPORT_DIR}" --deployment bundled ${release_arguments} ${arguments}
    )

    add_custom_target(create-apk-libs-${QTANDROID_EXPORTED_TARGET}
            COMMAND cmake -E remove_directory "${EXPORT_LIBS_DIR}"
            COMMAND cmake -E make_directory   "${EXPORT_LIBS_DIR}" )
    
    foreach(JAR_FILE ${LPKG_ANDROIDDEPLOYQT_JARS})
      MATH(EXPR COUNTER "${COUNTER}+1")
      add_custom_target(copy-jar-${QTANDROID_EXPORTED_TARGET}-${COUNTER}
            DEPENDS create-apk-libs-${QTANDROID_EXPORTED_TARGET}
            COMMAND cmake -E copy "${JAR_FILE}" "${EXPORT_LIBS_DIR}")
      add_dependencies(${CREATEAPK_TARGET_NAME} copy-jar-${QTANDROID_EXPORTED_TARGET}-${COUNTER})
    endforeach()
        
    add_custom_target(install-apk-${QTANDROID_EXPORTED_TARGET}
        COMMAND adb install -r "${EXPORT_DIR}/build/outputs/apk/debug/${QTANDROID_EXPORTED_TARGET}_build_apk-debug.apk"
    )
    
    if (NOT TARGET create-apks)
        add_custom_target(create-apks)
    endif()
    
    if (NOT TARGET install-apks)
        add_custom_target(install-apks)
    endif()
    
    add_dependencies(create-apks ${CREATEAPK_TARGET_NAME})
    add_dependencies(install-apks install-apk-${QTANDROID_EXPORTED_TARGET})
endfunction()
