 
if RUBY_VERSION.to_f >= 2.2
  require 'etc'
end

module ProcessorCount
  # Number of processors seen by the OS and used for process scheduling.
  #
  # * AIX: /usr/sbin/pmcycles (AIX 5+), /usr/sbin/lsdev
  # * BSD: /sbin/sysctl
  # * Cygwin: /proc/cpuinfo
  # * Darwin: /usr/bin/hwprefs, /usr/sbin/sysctl
  # * HP-UX: /usr/sbin/ioscan
  # * IRIX: /usr/sbin/sysconf
  # * Linux: /proc/cpuinfo
  # * Minix 3+: /proc/cpuinfo
  # * Solaris: /usr/sbin/psrinfo
  # * Tru64 UNIX: /usr/sbin/psrinfo
  # * UnixWare: /usr/sbin/psrinfo
  #
  def ProcessorCount.processor_count()
    if defined?(Etc) && Etc.respond_to?(:nprocessors)
      return Etc.nprocessors
    else
      os_name = RbConfig::CONFIG["target_os"]
      if os_name =~ /mingw|mswin/
        require 'win32ole'
        result = WIN32OLE.connect("winmgmts://").ExecQuery(
          "select NumberOfLogicalProcessors from Win32_Processor")
        return result.to_enum.collect(&:NumberOfLogicalProcessors).reduce(:+)
      elsif File.readable?("/proc/cpuinfo")
        return IO.read("/proc/cpuinfo").scan(/^processor/).size
      elsif File.executable?("/usr/bin/hwprefs")
        return IO.popen("/usr/bin/hwprefs thread_count").read.to_i
      elsif File.executable?("/usr/sbin/psrinfo")
        return IO.popen("/usr/sbin/psrinfo").read.scan(/^.*on-*line/).size
      elsif File.executable?("/usr/sbin/ioscan")
        return IO.popen("/usr/sbin/ioscan -kC processor") do |out|
          out.read.scan(/^.*processor/).size
        end
      elsif File.executable?("/usr/sbin/pmcycles")
        return IO.popen("/usr/sbin/pmcycles -m").read.count("\n")
      elsif File.executable?("/usr/sbin/lsdev")
        return IO.popen("/usr/sbin/lsdev -Cc processor -S 1").read.count("\n")
      elsif File.executable?("/usr/sbin/sysconf") and os_name =~ /irix/i
        return IO.popen("/usr/sbin/sysconf NPROC_ONLN").read.to_i
      elsif File.executable?("/usr/sbin/sysctl")
        return IO.popen("/usr/sbin/sysctl -n hw.ncpu").read.to_i
      elsif File.executable?("/sbin/sysctl")
        return IO.popen("/sbin/sysctl -n hw.ncpu").read.to_i
      else
        $stderr.puts "Unknown platform: " + RbConfig::CONFIG["target_os"]
        $stderr.puts "Assuming 1 processor."
        return 1
      end
    end
  end
end
