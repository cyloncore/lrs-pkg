class Package
  attr_reader :name, :short_description, :category, :depends, :optional_depends, :url, :vc, :recipe, :env_definitions, :patches
  def initialize(name, short_description, category, depends, optional_depends, url, vc, recipe)
    @name               = name
    @short_description  = short_description
    @category           = category
    @depends            = depends
    @optional_depends   = optional_depends
    @url                = url
    @vc                 = vc
    @vc_instance        = nil
    @recipe             = recipe
    @recipe_instance    = {}
    @options            = {}
    @env_definitions    = {}
    @patches            = []
  end
  def checkout_directory
    return File.expand_path("#{ENV['CC_PKG_ROOT']}/src/#{name}")
  end
  def debug_build_directory(prefix)
    return File.expand_path("#{ENV['CC_PKG_ROOT']}/#{prefix}build-debug/#{name}")
  end
  def release_build_directory(prefix)
    return File.expand_path("#{ENV['CC_PKG_ROOT']}/#{prefix}build-release/#{name}")
  end
  def checked_out?
    return File.directory?(checkout_directory)
  end
  def vc_instance
    if(@vc_instance.nil?)
      options = {}
      if(@options.include?(:vc))
        options = @options[:vc]
      end
      @vc_instance = @vc.new(@url, self.checkout_directory, options, self.patches)
    end
    return @vc_instance
  end
  def recipe_instance(platform, build_mode)
    unless(@recipe_instance.include?([platform, build_mode]))
      @recipe_instance[[platform, build_mode]] = @recipe.new(self, platform, build_mode)
    end
    return @recipe_instance[[platform, build_mode]]
  end
  def add_patch(*args)
    @patches.push(args.join("/"))
    return self
  end
  def add_option(key, value)
    keys = key.split(".").map { |s| s.to_sym }
    options(*keys[0...(keys.length-1)])[keys[keys.length-1]] = value
    return self
  end
  def option(*keys, default_value: nil)
    opt = options(*keys[0...(keys.length-1)])
    if opt.include?(keys[keys.length-1])
      return opt[keys[keys.length-1]]
    else
      return default_value
    end
  end
  def options(*keys)
    opt = @options
    keys.each() do |k|
      unless(opt.include?(k))
        opt[k] = {}
      end
      opt = opt[k]
    end
    return opt
  end
  def add_env_defininiton(key, value)
    env_definitions[key] = value
  end
  def is_enabled?(platform)
    opt1 = self.option(:platform, platform.name.to_sym, :enabled)
    opt2 = self.option(:platform, platform.group.to_sym, :enabled)
    return ((opt1.nil? or opt1) and (opt2.nil? or opt2))
  end
  ##
  # Return the list of installed dependencies, including optional dependencies.
  #
  def all_installed_dependencies()
    dependencies = @depends.clone()

    @optional_depends.each() do |x|
      if(Packages.get_package(x).checked_out?)
        dependencies << x
      end
    end
    return dependencies
  end
end
