export CC_PKG_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../.."

source $CC_PKG_ROOT/cc-pkg/shells/config.sh

munge () {
    if [[ ":${!1}:" != *:"$2":* ]]; then
        if [[ $3 == after ]]; then
            declare -g $1="${!1}:$2"
        else
            declare -g $1="$2:${!1}"
        fi
    fi
}

munge PKG_CONFIG_PATH $CC_PKG_INST/lib/pkgconfig before
