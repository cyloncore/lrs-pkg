export CC_PKG_ROOT=$(cd -P -- "$(dirname -- "$0")"/../.. && printf '%s\n' "$(pwd -P)")

source $CC_PKG_ROOT/cc-pkg/shells/config.sh

source $CC_PKG_ROOT/cc-pkg/shells/zsh/setup

export PKG_CONFIG_PATH=$CC_PKG_INST/lib/pkgconfig:$PKG_CONFIG_PATH
