# No reason to change the following lines
export CC_PKG_INST=$CC_PKG_ROOT/inst
export PATH=$CC_PKG_ROOT/cc-pkg:$CC_PKG_INST/bin:$PATH
export LD_LIBRARY_PATH=$CC_PKG_INST/lib:$LD_LIBRARY_PATH
export CMAKE_PREFIX_PATH=$CC_PKG_INST:$CMAKE_PREFIX_PATH

# QML2 related paths
export QML2_IMPORT_PATH=$CC_PKG_INST/lib/qt5/qml:$CC_PKG_INST/lib/qt6/qml

# Python path
PYTHON3_VERSION=`python3 -c 'import sys; print("{}.{}".format(sys.version_info[0], sys.version_info[1]))'`
export PYTHONPATH=$CC_PKG_INST/lib/python${PYTHON3_VERSION}/dist-packages:${PYTHONPATH}

# Ruby
export RUBYLIB=$CC_PKG_INST/lib/ruby:$RUBYLIB

# March related exports
export MARCH_PROTOTYPES_LIBRARIES_PATH=$MARCH_PROTOTYPES_LIBRARIES_PATH:$CC_PKG_INST/share/march/definitions/

`cc-pkg env`

if [ -v CC_PKG_USE_CARGO_TARGET_DIR ] && [ "$CC_PKG_USE_CARGO_TARGET_DIR" -eq "1" ]; then
  export CARGO_TARGET_DIR=$CC_PKG_ROOT/rust-build
fi
