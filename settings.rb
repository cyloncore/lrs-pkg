require 'yaml'

class Object
  def deep_symbolize_keys
    return self.inject({}){|memo,(k,v)| memo[k.to_sym] = v.deep_symbolize_keys; memo} if self.is_a? Hash
    return self.inject([]){|memo,v    | memo           << v.deep_symbolize_keys; memo} if self.is_a? Array
    return self
  end
end

module Settings
  extend self
  
  @_settings = {}
  attr_reader :_settings

  def load!(filename, options = {})
    if(File.exist?(filename))
      newsets = YAML::load_file(filename).deep_symbolize_keys
      newsets = newsets[options[:env].to_sym] if options[:env] and newsets[options[:env].to_sym]
      deep_merge!(@_settings, newsets)
    end
  end
  
  def save!(filename)
    File.open(filename, 'w') { |f| YAML.dump(@_settings, f) }
  end

  def deep_merge!(target, data)
    merger = proc{|key, v1, v2|
      Hash === v1 && Hash === v2 ? v1.merge(v2, &merger) : v2 }
    target.merge! data, &merger
  end

  def get_entries(sname)
    unless(@_settings.include?(sname))
      @_settings[sname] = {}
    end
    return @_settings[sname]
  end
  def method_missing(name, *args, &block)
    return get_entries(name.to_sym)
  end

end
