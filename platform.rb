class Platform
  def Platform.check_env()
  end
  def Platform.cmake_arguments(recipe)
  end
  def Platform.update_env(env)
  end
  def Platform.build_prefix()
    return self.name() + "-"
  end
  def Platform.install_dir()
    return "#{ENV['CC_PKG_ROOT']}/#{self.name()}-inst"
  end
  def Platform.is_enabled?(package)
    opt = package.option([:platform, self.name.to_sym, :enabled])
    return (opt.nil? or opt)
  end
  def Platform.cmake_release_mode()
    return "RelWithDebInfo"
  end
  def Platform.group()
    return "platform"
  end
protected
 def check_env_variable(name)
   unless(ENV.has_key?(name))
    Errors.invalid_environment(name)
   end
 end
end
