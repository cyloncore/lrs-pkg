require_relative 'platform'

AvailablePlatforms = {}

require_relative "platforms/host"
require_relative "platforms/android"
require_relative "platforms/ios"
require_relative "platforms/ios_simulator" 
