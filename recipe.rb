module BuildMode
  DEBUG         = 1
  RELEASE       = 2
end

class Recipe
  attr_reader :package, :build_mode, :build_dir, :platform
  def initialize(package, platform, build_mode)
    @package    = package
    @platform   = platform
    @build_mode = build_mode
    if(build_mode == BuildMode::DEBUG)
      @build_dir = package.debug_build_directory(platform.build_prefix)
    else
      @build_dir = package.release_build_directory(platform.build_prefix)
    end
  end
  def configured?
    return File.directory?(@build_dir)
  end
  def configure
    run_command("mkdir -p #{@build_dir}")
  end
  def build
  end
  def install
  end
  def has_tests?
    return false
  end
  def run_tests
  end
  def has_install?
    return self.package.options(:recipe, :disable_install) != true
  end
  def run_command(command, env = {})
    self.platform.update_env(env)
    Helpers.run_command(command, env)
  end
  def Recipe.call(command, package)
  end
end
