class Command
  def Command.name
    puts "Invalid command, missing name"
    exit -1
  end
  def Command.short_description
    puts "Invalid command, missing short description"
    exit -1
  end
  def Command.parse_options(parser, options)
  end
  def Command.options_set_descriptions(parser, instance, descriptions, banner_extra)
    parser.program_name += " " + instance.name
    parser.banner += " " + banner_extra
    parser.separator ""
    parser.separator instance.short_description + "."
    parser.separator ""
    descriptions.each() do |desc|
      parser.separator desc
    end
    parser.separator ""
    parser.separator "Sepcific options:"

  end
end
