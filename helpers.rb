module Helpers
  def Helpers.run_command(command, env = {})
    env_str = ""
    env.each do |key,value|
      env_str += "export #{key}=\"#{value}\";"
    end
    exitstatus = nil
    output = nil

    PTY.spawn("#{env_str}#{command}") do |reader, writer, pid|
      lines = []
      while $?.nil? or $?.pid != pid
        begin
          lines.append reader.readline
        rescue
        end
        Process.wait(pid, Process::WNOHANG)
      end
      unless $?.exitstatus == 0
        begin
          lines.each() { |l| puts l }
          until reader.eof?
            puts reader.readline
          end
        rescue
          puts "\n\n**** Failure ****\n\n\n".red
          puts "Command '#{command}' has failed, see output above."
          exit -1
        end
      end
    end
  end
end

require_relative "helpers/build"
