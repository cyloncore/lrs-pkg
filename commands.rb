require_relative 'command'

AvailableCommands = {}

def register_command(command)
  AvailableCommands[command.name] = command
end

require_relative "commands/build"
require_relative "commands/call"
require_relative "commands/clean"
require_relative "commands/config"
require_relative "commands/env"
require_relative "commands/get"
require_relative "commands/help"
require_relative "commands/info"
require_relative "commands/package"
require_relative "commands/status"
require_relative "commands/list"
require_relative "commands/update"
require_relative "commands/test"
