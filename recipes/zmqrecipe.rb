module Recipes

  class ZMQRecipe < Recipe
    def initialize(package, platform, build_mode)
      super
    end
    def configured?
      return (super and File.exists?("#{self.build_dir}/Makefile"))
    end
    def configure
      super
      run_command("cd #{self.package.checkout_directory}; ./autogen.sh; cd #{self.build_dir}; #{self.package.checkout_directory}/configure --prefix=#{self.platform.install_dir} #{configure_parameters} #{configure_extra_parameters}")
    end
    def build
      super
      run_command("cd #{self.build_dir}; make #{make_extra_parameters}")
    end
    def install
      super
      run_command("cd #{self.build_dir}; make install #{make_extra_parameters}")
    end
  protected
    def make_extra_parameters
      mep = ""
      self.package.options(:recipe, :make).each() do |k,v|
        unless(v.nil?)
          mep += " #{k}=\"#{v}\""
        end
      end
      return mep
    end
    def configure_extra_parameters
      cep = ""
      self.package.options(:recipe, :configure).each() do |k,v|
        unless(v.nil?)
          cep += " --#{k}=#{v}"
        end
      end
      return cep
    end
  private
    def configure_parameters
      if(self.build_mode == BuildMode::DEBUG)
        return "--enable-debug"
      else
        return "--disable-debug"
      end
    end
  end
end
