module Recipes

  class CmakeRecipe < Recipe
    def initialize(package, platform, build_mode)
      super
      if(Settings.cmake[:parallel_jobs] == nil)
        Settings.cmake[:parallel_jobs] = ProcessorCount.processor_count() + 1
      end
      if(Settings.cmake[:generator] == nil)
        Settings.cmake[:generator] = "'-GUnix Makefiles'"
      end
    end
    def configured?
      return (super and File.exist?("#{self.build_dir}/CMakeCache.txt") and File.exist?("#{self.build_dir}/Makefile"))
    end
    def configure
      super
      generator = ""
      run_command("cd #{self.build_dir}; cmake #{Settings.cmake[:generator]} -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_INSTALL_PREFIX=#{self.platform.install_dir} #{cmake_parameters} #{cmake_extra_parameters} #{self.package.checkout_directory} #{self.platform.cmake_arguments(self)}")
    end
    def build
      super
      run_command("cmake --build #{self.build_dir} -- -j#{parallel_jobs}")
    end
    def install
      super
      run_command("cmake --build #{self.build_dir} -- -j#{parallel_jobs} install")
    end
    def has_tests?
      return false if(self.package.options(:recipe)[:disable_tests] == true)
      file = File.open("#{self.build_dir}/Makefile", "r")
      return /test:\n/.match(file.read)
    end
    def run_tests
      super
      run_command("cd #{self.build_dir}; make test")
    end
  protected
    def parallel_jobs
      pj = self.package.option(:recipe, :parallel_jobs)
      return pj unless pj.nil?
      return Settings.cmake[:parallel_jobs]
    end
    def cmake_extra_parameters
      cep = ""
      self.package.options(:recipe, :cmake).each() do |k,v|
        unless(v.nil?)
          cep += " -D#{k}=#{v}"
        end
      end
      self.package.options(:recipe, self.platform.name().to_sym, :cmake).each() do |k,v|
        unless(v.nil?)
          cep += " -D#{k}=#{v}"
        end
      end
      return cep
    end
  private
    def cmake_parameters
      if(self.build_mode == BuildMode::DEBUG)
        return "-DCMAKE_BUILD_TYPE=Debug"
      else
        rbm = self.package.options(:recipe, :cmake_extra)[:release_mode]
        rbm = self.platform.cmake_release_mode() unless rbm
        return "-DCMAKE_BUILD_TYPE=#{rbm}"
      end
    end
  end
end
