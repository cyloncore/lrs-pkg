module Recipes

  class QmakeRecipe < Recipe
    def initialize(package, platform, build_mode)
      super(package, platform, build_mode)
      @min_version       = self.package.options(:recipe, :qmake, :MIN_VERSION)
      if @min_version.is_a? String
        version_parts = (/([0-9]+)\.([0-9]+)\.([0-9]+)/.match(@min_version))
        @min_major_version = version_parts[1].to_i
        @min_minor_version = version_parts[2].to_i
        @min_point_version = version_parts[3].to_i
      else
        @min_major_version = 5
        @min_minor_version = 0
        @min_point_version = 0
      end
      if(Settings.qmake[:parallel_jobs] == nil)
        Settings.qmake[:parallel_jobs] = ProcessorCount.processor_count() + 1
      end
    end
    def configured?
      return (super and File.exists?("#{self.build_dir}/Makefile"))
    end
    def configure
      super
      qmake_cmd = "qmake"
      
      if(not self.check_version(qmake_cmd))
        qmake_cmd = "qtchooser -run-tool=qmake -qt=#{@min_major_version}"
        if(not self.check_version(qmake_cmd))
          puts "Could not find Qt >= #{@min_version}".red()
          exit -1
        end
      end
      
      run_command("cd #{self.build_dir}; #{qmake_cmd} target.path=#{self.platform.install_dir} PREFIX=#{self.platform.install_dir} #{qmake_parameters} #{qmake_extra_parameters} #{self.package.checkout_directory}")
    end
    def build
      super
      run_command("cd #{self.build_dir}; make -j#{Settings.qmake[:parallel_jobs]}")
    end
    def install
      super
      run_command("cd #{self.build_dir}; make -j#{Settings.qmake[:parallel_jobs]} install")
    end
  protected
    def check_version(qmake_cmd)
      version_parts = /Using Qt version ([0-9]+)\.([0-9]+)\.([0-9]+) in .*/.match(`#{qmake_cmd} --version`)
      major_version = version_parts[1].to_i
      minor_version = version_parts[2].to_i
      point_version = version_parts[3].to_i
      return (major_version == @min_major_version and (minor_version > @min_minor_version or (minor_version == @min_minor_version and point_version >= @min_point_version)))
    end
    def qmake_extra_parameters
      cep = ""
      self.package.options(:recipe, :qmake).each() do |k,v|
        unless(v.nil?)
          cep += " #{k}=#{v}"
        end
      end
      return cep
    end
  private
    def qmake_parameters
      if(self.build_mode == BuildMode::DEBUG)
        return "CONFIG+=debug"
      else
        return "CONFIG+=release"
      end
    end
  end
end
