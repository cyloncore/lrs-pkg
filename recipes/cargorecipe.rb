module Recipes

  class CargoRecipe < Recipe
    def initialize(package, platform, build_mode)
      super
    end
    def configured?
      return super
    end
    def configure
      super
    end
    def build
      run_command("cd #{self.package.checkout_directory}; CARGO_TARGET_DIR=#{ENV['CC_PKG_ROOT']}/rust-build cargo build #{cargo_parameters}")
      super
    end
    def install
      self.package.options(:recipe)[:install_paths].each do |x|
        run_command("cd #{self.package.checkout_directory}; CARGO_TARGET_DIR=#{ENV['CC_PKG_ROOT']}/rust-build cargo install --bins --root #{ENV['CC_PKG_ROOT']}/inst/ --path #{x}")
      end
      super
    end
    def CargoRecipe.call(package, command)
      if command == "cargo-upgrades"
        Helpers.run_command("cd #{package.checkout_directory}; cargo upgrades", {})
      end
    end
  private
    def cargo_parameters
      if(self.build_mode == BuildMode::DEBUG)
        return "--profile dev"
      else
        return "--profile release"
      end
    end
  end
end
