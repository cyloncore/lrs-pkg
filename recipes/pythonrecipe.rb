 
module Recipes

  class PythonRecipe < Recipe
    def initialize(package, platform, build_mode)
      super
    end
    def configured?
      return super
    end
    def configure
      super
    end
    def build
      super
    end
    def install
      super
    end
  end
end
