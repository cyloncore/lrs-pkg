require_relative 'package'
require_relative 'versioncontrol'

UAvailablePackages = {}

def create_package(name, short_description, category, depends, optional_depends, url, vc, recipe)
#   depends.each() do |d|
#     unless(UAvailablePackages.include?(d))
#       Errors.unknown_package_dependency(d, name)
#     end
#   end
  UAvailablePackages[name] = Package.new(name, short_description, category, depends, optional_depends, url, vc, recipe)
end

class Repository
  attr_reader :name, :directory
  def initialize version_control, directory, name
    @version_control = version_control
    @vc_instance = nil
    @directory = directory
    @name = name
    @dependencies = []
  end
  def name= name
    @name = name
  end
  def add_depend vc, url, push_url: nil
    @dependencies.append({:vc => vc, :url => url, :push_url => push_url })
  end
  def update
    if File.basename(@directory) != @name
      dst_dir = " #{File.dirname(@directory)}/#{@name}"
      `mv #{@directory} #{dst_dir}`
      @directory =dst_dir
    end
    @dependencies.each() do |x|
      Repositories.require_dependency x[:vc], x[:url], x[:push_url]
    end
  end
  def vc_instance
    if(@vc_instance.nil?)
      @vc_instance = @version_control.new("", self.directory, {}, [])
    end
    return @vc_instance
  end

end

module Repositories
  def Repository.current_repository
    return @@current_repository
  end
  # def Repository.set_name name
  # end
  def Repositories.require_dependency vc, url, push_url
    unless Repositories.has_dependency? url, push_url
      options = {}
      options[:push_url] = push_url if push_url != nil
      dst_dir = File.dirname(__FILE__) + "/repositories/tmp" + rand(1000).to_s
      vc.new(url, dst_dir,  options, []).clone()
      Repositories.load_repository dst_dir, true, true, true
    end
  end
  def Repositories.has_dependency? url, push_url
    @@repositories.each() do |key, repo|
      if repo.vc_instance.remote_url == url or repo.vc_instance.remote_url == push_url
        return true
      end
    end
    return false
  end
  def Repositories.load_repository directory, init_object, load_repo, update
    if init_object
      name = File.basename directory
      if(File.exist?("#{directory}/.hg"))
        vc = Hg
      elsif(File.exist?("#{directory}/.git"))
        vc = Git
      end
      repository = Repository.new vc, directory, name
      @@repositories[directory] = repository

    else
      repository = @@repositories[directory]
    end
    if load_repo
      @@current_repository = repository
      require "#{directory}/packages.rb"
      @@current_repository = nil
    end
    if update
      repository.update
      if repository.directory != directory
        @@repositories.delete directory
        @@repositories[repository.directory] = repository
      end
    end
  end
  def Repositories.load_repositories
    @@repositories = {}
    Dir[File.dirname(__FILE__) + "/repositories/*/"].each do |f|
      Repositories.load_repository f, true, false, false
    end
    Dir[File.dirname(__FILE__) + "/repositories/*/"].each do |f|
      Repositories.load_repository f, false, true, true
    end
  end
  def Repositories.repositories
    return @@repositories
  end
end

Repositories.load_repositories

AvailablePackages = Hash[UAvailablePackages.sort]

module Packages
  def Packages.list_checked_out_package
    list = []
    AvailablePackages.each() do |name,package|
      if(package.checked_out?)
        list << package
      end
    end
    return list
  end
  def Packages.get_package(package_name)
    if(AvailablePackages.include?(package_name))
      return AvailablePackages[package_name]
    else
      Errors.unknown_package(package_name)
    end
  end
  def Packages.get_packages(packages_names)
    packages = []
    packages_names.each() do |name|
      packages << Packages.get_package(name)
    end
    return packages
  end
end

