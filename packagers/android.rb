module Packagers
  class Android < Package
    def Android.check()
    end
    def Android.package(package)
      androidPlatforms = ['android32', 'android64']
      androidPlatforms.each() do |platformName|
        `rm -rf #{ENV['CC_PKG_ROOT']}/#{platformName}-build-release/ #{ENV['CC_PKG_ROOT']}/#{platformName}-inst/`
        platform = AvailablePlatforms[platformName]
        builder = Helpers::Build.new(platform, BuildMode::RELEASE, false)
        builder.recursive_build(package)
        recipe = package.recipe_instance(platform, BuildMode::RELEASE)
        recipe.run_command("cd #{recipe.build_dir}; make -j#{Settings.cmake[:parallel_jobs]} create-apks")
        recipe.run_command("cp #{recipe.build_dir}/#{package.name}_build_apk//build/outputs/apk/release/#{package.name}_build_apk-release-unsigned.apk #{Android.make_apk_name(platform, package, "-unsigned")}")
      end
      password = nil
      owner_uri = package.options(:packager, :android, :owner_uri)
      keystore = "~/.android/#{owner_uri}.keystore"
      while(password.nil?)
        print 'Password: '.blue
        system 'stty -echo'
        password = $stdin.gets.chomp
        system 'stty echo'
        print "\n"
        `keytool -list -keystore #{keystore} -storepass \"#{password}\"`
        if($?.exitstatus != 0)
          puts "Wrong password!".red
          password = nil
        end
      end
      
      androidPlatforms.each() do |platformName|
        platform = AvailablePlatforms[platformName]
        Helpers.run_command("jarsigner -verbose -sigalg SHA1withRSA -storepass \"#{password}\" -digestalg SHA1 -keystore #{keystore}  #{Android.make_apk_name(platform, package, "-unsigned")} #{owner_uri}")
        Helpers.run_command("#{ENV['ANDROID_SDK_ROOT']}/build-tools/26.0.3/zipalign -f -v 4 #{Android.make_apk_name(platform, package, "-unsigned")} #{Android.make_apk_name(platform, package, "-signed")}")
      end
    end
    protected
    def Android.make_apk_name(platform, package, suffix)
      return "#{package.name}-#{platform.name}#{suffix}.apk"
    end
  end
end

AvailablePackagers['android'] = Packagers::Android
