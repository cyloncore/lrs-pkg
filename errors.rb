module Errors
  def Errors.invalid_command_usage(command)
    puts "Invalid usage of '#{command}' command. Use 'cc-pkg help #{command}' to know more about this command."
    exit -1
  end
  def Errors.invalid_option(argument)
    puts "Invalid argument '#{argument}'"
    exit -1
  end
  def Errors.package_already_checked_out(packages)
    if(packages.size > 1)
      puts "Packages (#{packages.join(",")}) have already been checked out."
    else
      puts "Package (#{packages[0]}) has already been checked out."
    end
    exit -1
  end
  def Errors.unknown_command(command)
    puts "Unknown command '#{command}'. Use 'cc-pkg help' to see the list of available commands."
    exit -1;
  end
  def Errors.unknown_option(option, command)
    puts "Unknown option '#{option}'. Use 'cc-pkg help #{command}' to see the list of available options for the command '#{command}'."
    exit -1;
  end
  def Errors.unknown_package(package)
    puts "Unknown package '#{package}'. Use 'cc-pkg list' to see the list of available packages."
    exit -1;
  end
  def Errors.unknown_packager(packager)
    puts "Unknown packager '#{packager}'."
    exit -1;
  end
  def Errors.unknown_package_dependency(d_name, package_name)
    puts "Unknown package '#{d_name}' marked as a dependency of '#{package_name}'"
    exit -1
  end
  def Errors.package_is_not_build(package_name)
    puts "Package '#{package_name}' is not built."
    exit -1
  end
  def Errors.invalid_environment(missing)
    puts "Invalid environment missing '#{missing}'"
    exit -1
  end
end
