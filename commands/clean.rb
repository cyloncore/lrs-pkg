
module Commands
  class Clean < Command
    def initialize(options)
      @options = options
    end
    def execute()
      if(@options.args.count == 0)
        `rm -rf #{ENV['CC_PKG_ROOT']}/rust-build #{ENV['CC_PKG_ROOT']}/*-build-debug/ #{ENV['CC_PKG_ROOT']}/*-build-release/ #{ENV['CC_PKG_ROOT']}/inst/`
        puts "Success!".green
      elsif(@options.args[0] == "install")
        `rm -rf #{ENV['CC_PKG_ROOT']}/inst/`
        puts "Success!".green
      elsif(@options.args[0] == "builds")
        `rm -rf #{ENV['CC_PKG_ROOT']}/rust-build #{ENV['CC_PKG_ROOT']}/*-build-debug/ #{ENV['CC_PKG_ROOT']}/*-build-release/`
        puts "Success!".green
      elsif(@options.args[0] == "build-release")
        `rm -rf #{ENV['CC_PKG_ROOT']}/*-build-release/`
        puts "Success!".green
      elsif(@options.args[0] == "build-debug")
        `rm -rf #{ENV['CC_PKG_ROOT']}/*-build-debug/`
        puts "Success!".green
      elsif(@options.args[0] == "rust-build")
        `rm -rf #{ENV['CC_PKG_ROOT']}/*-rust-build/`
        puts "Success!".green
      else
        Errors.invalid_command_usage('clean')
      end
    end
    def Clean.name
      return "clean"
    end
    def Clean.short_description
      return "clean the builds and install directory"
    end
    def Clean.help_message
      return 
    end
    def Clean.parse_options(parser, options)
      Command.options_set_descriptions(parser, Clean, [ <<GET_HELP
This command clean the builds and install directory:

cc-pkg clean

This command clean the install directory:

cc-pkg clean install

This command clean the builds directory:

cc-pkg clean builds

This command clean the release build directory:

cc-pkg clean build-release

This command clean the debug build directory:

cc-pkg clean build-debug

GET_HELP
         ], "[command]")
    end
  end
end

register_command(Commands::Clean)
