
module Commands
  class List < Command
    def initialize(options)
      @options = options
    end
    def execute()
      if(@options.args.count == 0)
        if(@options.single_line)
          puts AvailablePackages.keys().join(" ")
        else
          AvailablePackages.each() do |k,v|
            if(v.checked_out?)
              status = "c"
            else
              status = " "
            end
            printf "[%s] %-30s %s\n", status, k, v.short_description
          end
        end
      else
        Errors.invalid_command_usage('list')
      end
    end
    def List.name
      return "list"
    end
    def List.short_description
      return "list the available packages"
    end
    def List.help_message
      return <<GET_HELP
This command list all packages:

cc-pkg list

If you want the list on a single line:

cc-pkg list --single-line

GET_HELP
    end
    def List.parse_options(parser, options)
      Command.options_set_descriptions(parser, List, [], "")
      options.single_line = false
      parser.on("--single-line") do |csl|
        options.single_line = csl
      end
    end
  end
end

register_command(Commands::List)
