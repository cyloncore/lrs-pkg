
module Commands
  class Test < Command
    def initialize(options)
      @options = options
    end
    def execute()
      if(@options.args.count == 0)
        list_checked_out_package = Packages.list_checked_out_package()
        list_checked_out_package.each() do |package|
          run_test(package, true)
        end
      elsif(@options.args.count > 0)
        packages = Packages.get_packages(@options.args)
        packages.each() do |package|
          run_test(package, false)
        end
        puts "Success!".green
      else
        Errors.invalid_command_usage('test')
      end
    end
  private
    def run_test(package, ignore_not_build)
      # Now test the current package
      host_platform = AvailablePlatforms["host"]
      recipe = package.recipe_instance(host_platform, @options.test)
      recipe = package.recipe_instance(host_platform, BuildMode::DEBUG) unless(recipe.configured?)
      recipe = package.recipe_instance(host_platform, BuildMode::RELEASE) unless(recipe.configured?)
      if(recipe.configured?)
        if(recipe.has_tests?)
          puts "Testing #{package.name}"
          recipe.run_tests
        end
      else
        unless(ignore_not_build)
          Errors.package_is_not_build(package.name)
        end
      end
    end
  public
    def Test.name
      return "test"
    end
    def Test.short_description
      return "Run the tests of packages."
    end
    def Test.parse_options(parser, options)
      Command.options_set_descriptions(parser, Test, [], " [optional packagenames]")
      options.test = BuildMode::RELEASE
      parser.on("--test type", "select the mode between 'debug' or 'release' (default to release)") do |build|
        if build == "debug"
          options.test = BuildMode::DEBUG
        else
          options.test = BuildMode::RELEASE
        end
      end
    end
  end
end

register_command(Commands::Test)
 
