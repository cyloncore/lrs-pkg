
module Commands
  class Get < Command
    def initialize(options)
      @options = options
    end
    def execute()
      if(not @options.repo.nil?)
        if(@options.args.count == 1)
          repo_url = @options.args[0]
          vcs = @options.vcs.new(repo_url, File.dirname(__FILE__) + "/../repositories/#{@options.repo}", {}, [])
          vcs.clone()
        else
          Errors.invalid_command_usage('get')
        end
      elsif(@options.args.count > 0)
        already_checked_out = []
        @options.args.each() do |package_name|
          package = Packages.get_package(package_name)
          if(package.checked_out?)
            already_checked_out.push(package_name)
          else
            recursive_check_out(package)
          end
        end
        if(not @options.ignore_checked_out and already_checked_out.size() > 0)
          Errors.package_already_checked_out(already_checked_out)
        end
      else
        Errors.invalid_command_usage('get')
      end
    end
  private
    def recursive_check_out(package)
      # Checkout dependencies
      package.depends().each() do |d_name|
        if(AvailablePackages.include?(d_name))
          recursive_check_out(AvailablePackages[d_name])
        else
          Errors.unknown_package_dependency(d_name, package.name)
        end
      end
      # Now checkout the actual package
      unless(package.checked_out?)
        puts "Checking out #{package.name}"
        package.vc_instance.clone()
      end
    end
  public
    def Get.name
      return "get"
    end
    def Get.short_description
      return "clone and checkout packages and repositories"
    end
    def Get.parse_options(parser, options)
      Command.options_set_descriptions(parser, Get, [], " [list of packagename or repository url]")
      options.ignore_checked_out = false
      options.repo = nil
      options.vcs = Git
      parser.on("--ignore-checked-out") do |ico|
        options.ignore_checked_out = ico
      end
      parser.on("--repo NAME", "Name of the repository to get") do |repo|
        options.repo = repo
      end
      parser.on("--vcs VCS", "Specify the version control system used to get the repository (default is git)") do |vcs|
        options.vcs = vcs
      end
    end
  end
end

register_command(Commands::Get)
