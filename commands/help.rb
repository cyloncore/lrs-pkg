
module Commands
  class Help < Command
    def initialize(options)
      @options = options
    end
    def execute()
      # No argument, then show the list of available commands
      if(@options.args.count == 0)
        
        if(@options.commands_single_line)
          puts AvailableCommands.keys.join(" ")
        else
          puts "List of available commands:"
          AvailableCommands.each() do |k,v|
            desc = v.short_description
            desc[0] = desc[0].downcase
            printf "%-20s %s\n", k, desc
          end
          puts "\nYou can use 'cc-pkg help [command_name]' to get more information about a specific command"
        end
      # Show the help specific to a single command
      elsif(@options.args.count == 1)
        command = @options.args[0]
        if(AvailableCommands.include?(command))
          options = OpenStruct.new
          opt_parser = OptionParser.new
          AvailableCommands[command].parse_options(opt_parser, options)
          puts opt_parser
        else
          Errors.unknown_command(command)
        end
      # Invalid use of the help command
      else
        Errors.invalid_command_usage('help')
      end
    end
    def Help.name
      return "help"
    end
    def Help.short_description
      return "provides help about cc-pkg usage"
    end
    def Help.parse_options(parser, options)
      options.commands_single_line = false
      parser.on("--commands-single-line") do |csl|
        options.commands_single_line = csl
      end
    end
  end
end

register_command(Commands::Help)
