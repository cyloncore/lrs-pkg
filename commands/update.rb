
module Commands
  module UpdateMode
    NONE          = 0
    SELF          = 1
    REPOSITORIES  = 2
    PACKAGES      = 4
    
    ALL           = SELF | REPOSITORIES | PACKAGES       
  end

  class Update < Command
    def initialize(options)
      @options = options
    end
    def execute()
      if(@options.args.count > 0 and @options.updateMode == UpdateMode::ALL)
        @options.updateMode = UpdateMode::PACKAGES
      end
      if(@options.updateMode & UpdateMode::SELF != 0)
        update_self()
      end
      if(@options.updateMode & UpdateMode::REPOSITORIES != 0)
        update_repositories()
      end
      if(@options.updateMode & UpdateMode::PACKAGES != 0)
        if(@options.args.count == 0)
          packages = Packages.list_checked_out_package()
        else
          packages = Packages.get_packages(@options.args)
        end
        packages.each() do |package|
          puts "Updating package #{package.name}".blue
          package.vc_instance.updateRemote()
          package.vc_instance.fetch()
          recursive_check_out(package)
        end
      end
      puts("Success!".green)
    end
    
    def update_self()
      puts "Updating package cc-pkg".blue
      repo = Git.new("", CC_PKG_DIR, {}, [])
      repo.fetch()
    end
    def update_repositories()
      Repositories.repositories.each() do |k, repo|
        puts "Updating repository #{repo.name}".blue
        repo.vc_instance.fetch()
      end
    end
  private
    def recursive_check_out(package)
      # Checkout dependencies
      package.depends().each() do |d_name|
        if(AvailablePackages.include?(d_name))
          recursive_check_out(AvailablePackages[d_name])
        else
          Errors.unknown_package_dependency(d_name, package.name)
        end
      end
      # Now checkout the actual package
      unless(package.checked_out?)
        puts "Checking out #{package.name}"
        package.vc_instance.clone()
      end
    end
  public
    def Update.name
      return "update"
    end
    def Update.short_description
      return "Update packages, cc-pkg and repositories."
    end
    def Update.parse_options(parser, options)
      options.updateMode = UpdateMode::ALL
      Command.options_set_descriptions(parser, Update, ["If a package names are specified on the command line, only those packages are updated."], " [optional package names]")
      parser.on("--self", "update only cc-pkg") do ||
        options.updateMode = UpdateMode::NONE if(options.updateMode == UpdateMode::ALL)
        options.updateMode |= UpdateMode::SELF
      end
      parser.on("--repositories", "update all repositories") do ||
        options.updateMode = UpdateMode::NONE if(options.updateMode == UpdateMode::ALL)
        options.updateMode |= UpdateMode::REPOSITORIES
      end
      parser.on("--packages", "update all packages") do ||
        options.updateMode = UpdateMode::NONE if(options.updateMode == UpdateMode::ALL)
        options.updateMode |= UpdateMode::PACKAGES
      end
      parser.banner += " [optional packages names]"
    end
  end
end

register_command(Commands::Update)
