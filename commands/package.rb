module Commands
  class Package < Command
    def initialize(options)
      @options = options
    end
    def execute()
      if(@options.args.count == 2)
        packager = AvailablePackagers[@options.args[0]]
        if(packager.nil?)
          Errors.unknown_packager(@options.args[0])
        end
        package = Packages.get_package(@options.args[1])
        packager.check()
        packager.package(package)
      else
        Errors.invalid_command_usage('package')
      end
    end
  public
    def Package.name
      return "package"
    end
    def Package.short_description
      return "Generate a installable package"
    end
    def Package.parse_options(parser, options)
      Command.options_set_descriptions(parser, Package, [], " [packager] [packagename]")
    end
  end
end

register_command(Commands::Package)
