module Commands
  class Info < Command
    def initialize(options)
      @options = options
    end
    def execute()
      if(@options.args.count == 1)
        packagename = @options.args[0]
        package = Packages.get_package(packagename)
        
        if(@options.src_dir)
          puts package.checkout_directory
        elsif(@options.build_debug_dir)
          puts package.debug_build_directory
        elsif(@options.build_release_dir)
          puts package.release_build_directory
        else
        end
      else
        Errors.invalid_command_usage('info')
      end
    end
    def Info.name
      return "info"
    end
    def Info.short_description
      return "get information about packages"
    end
    def Info.parse_options(parser, options)
      Command.options_set_descriptions(parser, Info, [], " [package name]")
      options.src_dir = false
      options.build_debug_dir = false
      options.build_release_dir = false
      parser.on("--src-dir", "return the source directory of a package") do |sd|
        options.src_dir = sd
      end
      parser.on("--build-debug-dir", "return the debug build directory of a package") do |sd|
        options.build_debug_dir = sd
      end
      parser.on("--build-release-dir", "return the release build directory of a package") do |sd|
        options.build_release_dir = sd
      end
    end
  end
end

register_command(Commands::Info)
