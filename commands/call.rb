
module Commands
  class Call < Command
    def initialize(options)
      @options = options
    end
    def execute()
      command_name = @options.args.shift
      if @options.args.count == 0
        packages = Packages.list_checked_out_package()
      else
        packages = Packages.get_packages(@options.args)
      end

      packages.each() do |package|
        package.recipe.call package, command_name
      end
    end
  private
  public
    def Call.name
      return "call"
    end
    def Call.short_description
      return "call a command specific to a build system"
    end
    def Call.parse_options(parser, options)
      Command.options_set_descriptions(parser, Call, ["If a package names are specified on the command line, only those packages (and their dependencies) are checked."], "[optional packages names]")
    end
  end
end

register_command(Commands::Call)
