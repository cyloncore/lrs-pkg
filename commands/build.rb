
module Commands
  class Build < Command
    def initialize(options)
      @options = options
    end
    def execute()
      build_helper = Helpers::Build.new(@options.platform, @options.build_mode, @options.force_configure, @options.even_explicits)
      
      if(@options.args.count == 0)
        list_checked_out_package = Packages.list_checked_out_package()
        list_checked_out_package.each() do |package|
          if @options.even_explicits or not package.option(:command, :build, :explicit, default_value: false)
            build_helper.recursive_build(package)
          end
        end
      elsif(@options.args.count == 1)
        packages = Packages.get_packages(@options.args)
        packages.each() do |package|
          if(@options.recursive)
            build_helper.recursive_build(package)
          else
            build_helper.build(package)
          end
        end
        puts "Success!".green
      else
        Errors.invalid_command_usage('build')
      end
    end
  private
  public
    def Build.name
      return "build"
    end
    def Build.short_description
      return "build and install packages"
    end
    def Build.parse_options(parser, options)
      Command.options_set_descriptions(parser, Build, ["If a package names are specified on the command line, only those packages (and their dependencies) are build."], "[optional packages names]")
      options.build_mode = BuildMode::RELEASE
      options.platform = AvailablePlatforms["host"]
      options.force_configure = false
      options.recursive = true
      options.even_explicits = false
      parser.on("--build-mode type", "Select the build mode, type is 'debug' or 'release'") do |build|
        if build == "debug"
          options.build_mode = BuildMode::DEBUG
        else
          options.build_mode = BuildMode::RELEASE
        end
      end
      parser.on("--platform name", "Build for the specified platform, by default it builds for 'host'") do |name|
        options.platform = AvailablePlatforms[name]
      end
      parser.on("--force-configure", "Use to force the configuration of the packages") do |fc|
        options.force_configure = fc
      end
      parser.on("--[no-]recursive", "This command build and install a specific package without the dependencies") do |fc|
        options.recursive = fc
      end
      parser.on("--even-explicits", "Also build explicits packages") do |fc|
        options.even_explicits = fc
      end
    end
  end
end

register_command(Commands::Build)
