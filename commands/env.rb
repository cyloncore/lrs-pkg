
module Commands
  class Env < Command
    def initialize(options)
      @options = options
    end
    def execute()
      list_checked_out_package = Packages.list_checked_out_package()
      list_checked_out_package.each() do |package|
        package.env_definitions.each() do |k,v|
          puts "export #{k}=#{v}"
        end
      end
    end
  public
    def Env.name
      return "env"
    end
    def Env.short_description
      return "Output environment variable defined by installed packages in sh format"
    end
    def Env.parse_options(parser, options)
      Command.options_set_descriptions(parser, Env, [], "")
    end
  end
end

register_command(Commands::Env)
