require 'pp'

module Commands
  class Config < Command
    def initialize(options)
      @already_build = []
      @options = options
    end
    def execute()
      if(@options.args.count >= 1)
        if(@options.args[0] == 'dump')
          pp(Settings._settings)
        elsif(@options.args[0] == 'set' and @options.args.count == 3)
          k = @options.args[1]
          k1 = k.split('.')[0]
          k2 = k.split('.')[1]
          value = @options.args[2]
          if value.start_with?("\\-")
            value = value.delete_prefix('\\')
          end
          Settings.get_entries(k1.to_sym)[k2.to_sym] = value
          Settings.save!("#{Dir.home()}/.lrspkgrc")
        elsif(@options.args[0] == 'get' and @options.args.count == 2)
          k = @options.args[1]
          k1 = k.split('.')[0]
          k2 = k.split('.')[1]
          pp(Settings.get_entries(k1.to_sym)[k2.to_sym])
        else
          Errors.invalid_command_usage('config')
        end
      else
        Errors.invalid_command_usage('config')
      end
    end
    def Config.name
      return "config"
    end
    def Config.short_description
      return "manipulate configs"
    end
    def Config.help_message
      return 
    end
    def Config.parse_options(parser, options)
      Command.options_set_descriptions(parser, Config, [<<GET_HELP
This command list all configs:

cc-pkg config dump

If you want to set a config value:

cc-pkg config set [key] [value]

If you want to get a config value:

cc-pkg config get [key]

GET_HELP
      ], "")
    end
  end
end

register_command(Commands::Config)
