
module Commands
  class Status < Command
    def initialize(options)
      @options = options
    end
    def execute()
      if(@options.args.count == 0)
        # Check cc-pkg
        check_status("", "cc-pkg", Git.new("", CC_PKG_DIR, {}, []))
        # Check repositories
        Repositories.repositories.each() do |k, repo|
          check_status("Repository ", k, repo.vc_instance)
        end
        
        # Check packages
        list_checked_out_package = Packages.list_checked_out_package()
        list_checked_out_package.each() do |package|
          check_status("Package ", package.name, package.vc_instance)
        end
      else
        Errors.invalid_command_usage('status')
      end
    end
  private
    def check_status(prefix, name, vc_instance)
      status   = vc_instance.has_uncommited()
      outgoing = vc_instance.has_outgoing()
      if(status and outgoing)
        puts "#{prefix}#{name} has uncommited and outgoing changes."
      elsif(outgoing)
        puts "#{prefix}#{name} has outgoing changes."
      elsif(status)
        puts "#{prefix}#{name} has uncommited changes."
      end
    end
  public
    def Status.name
      return "status"
    end
    def Status.short_description
      return "show status in packages (uncommited and outgoing)"
    end
    def Package.parse_options(parser, options)
      Command.options_set_descriptions(parser, Status, [], "")
    end
  end
end

register_command(Commands::Status)
